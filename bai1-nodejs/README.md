<h1>SERIES TỰ HỌC DOCKER - BÀI 1 - DOCKERIZE NODEJS</h1>

- [1. Mở đầu](#1-mở-đầu)
- [2. Cấu hình Dockerfile:](#2-cấu-hình-dockerfile)
  - [2.1. Giải thích file cấu hình bên trên:](#21-giải-thích-file-cấu-hình-bên-trên)
- [3. Build Docker Image:](#3-build-docker-image)
  - [3.1. Giải thích câu lệnh trên:](#31-giải-thích-câu-lệnh-trên)
- [4. Chạy project từ Docker Image:](#4-chạy-project-từ-docker-image)
  - [4.1. Giải thích:](#41-giải-thích)
---
# 1. Mở đầu 
Trước khi bắt đầu **dockerize** ứng dụng các bạn nhớ kiểm tra cài đặt **docker** và **docker compose** chưa nhé. Nếu chưa cài các bạn có thể xem hướng dẫn tại đây:
[Install docker ](https://docs.docker.com/engine/install/ubuntu/)
[Install docker compose ](https://docs.docker.com/compose/install/linux/#install-using-the-repository)
Version OS, Docker, Docker compose hiện tại đang dùng:
> - Ubuntu 22.04.1 LTS<br>
> - Docker version 20.10.21, build baeda1f<br>
> - Docker Compose version v2.12.2

Để tạo ra Image chúng ta sẽ viết cấu hình cho Image ở trong 1 file gọi là Dockerfile, trong đó sẽ đầy đủ chi tiết những thứ như: môi trường gì (ubuntu, win, Centos, debian,...), php mấy, database,....

Bắt tay vào làm thôi nhé.
# 2. Cấu hình Dockerfile:
Vẫn ở trong folder "bai1-nodejs", cả bài này chúng ta sẽ chỉ thao tác với folder này nhé. Các bạn tạo 1 file tên là Dockerfile với nội dung như sau:
```yml
FROM node:19-alpine
WORKDIR /app
COPY . .
RUN npm install
CMD ["npm", "start"] 
```
## 2.1. Giải thích file cấu hình bên trên:
- Ở đầu mỗi file Dockerfile ta luôn phải có FROM. FROM ý chỉ ta bắt đầu từ môi trường nào, môi trường này phải đã được build thành Image. Ở đây ta bắt đầu từ môi trường với Image có tên là node:19-alpine, đây là Image đã được build sẵn và chính thức (official) từ team của NodeJS, Ở môi trường này ta có sẵn phiên bản NodeJS mới nhất (latest), hiện tại là 13.2. Làm sao để lấy thông tin của Image thì mình lấy ở Docker hub nhé.
- Tiếp theo ta có từ khoá WORKDIR. Ý chỉ ở bên trong image này, tạo ra cho tôi folder tên là app và chuyển tôi đến đường dẫn /app. WORKDIR các bạn có thể coi nó tương đương với câu lệnh mkdir /app && cd /app (đường dẫn này các bạn có thể đặt tuỳ ý nhé, ở đây mình chọn là app)
- Tiếp theo ta có câu lệnh COPY và các bạn để ý thấy ta có 2 dấu "chấm", trông kì ta. 😄. Ý dòng này là: Copy toàn bộ code ở môi trường gốc, tức ở folder "bai1-nodejs" hiện tại vào bên trong Image ở đường dẫn /app. (chắc bạn sẽ thắc mắc vậy nếu muốn copy chỉ một hoặc một vài file thì sao, xem ở cuối bài nhé 😉)
- Tiếp theo ta có câu lệnh RUN. RUN để chạy một câu lệnh nào đó khi build Docker Image, ở đây ta cần chạy npm install để cài dependencies, như bao project NodeJS khác. Các bạn thật chú ý cho mình là RUN chạy khi BUILD thôi nhé, và vì chạy khi build nên nó chỉ được chạy 1 lần trong lúc build, chú ý điều này để thấy sự khác biệt với CMD mình nói phía dưới nhé.
- Tiếp theo là câu lệnh CMD. CMD để chỉ câu lệnh mặc định khi mà 1 container được khởi tạo từ Image này. CMD sẽ luôn được chạy khi 1 container được khởi tạo từ Image nhé 😉. CMD nhận vào 1 mảng bên trong là các câu lệnh các bạn muốn chạy, cứ 1 dấu cách thì ta viết riêng ra nhé. Ví dụ như: CMD ["npm", "run", "dev"] chẳng hạn
- Chú ý là một Dockerfile CHỈ ĐƯỢC CÓ DUY NHẤT 1 CMD.
# 3. Build Docker Image:
Để build image trong Docker bằng Dockerfile ta chạy lệnh sau:
**docker build -t learning-docker/node:v1 .**
## 3.1. Giải thích câu lệnh trên:
- Để build Docker image ta dùng command *docker build...
- Option -t để chỉ là đặt tên Image là như thế này cho tôi, và sau đó là tên của image. Các bạn có thể không cần đến option này, nhưng như thế build xong ta sẽ nhận được 1 đoạn code đại diện cho image, và chắc là ta sẽ quên nó sau 3 giây. Nên lời khuyên của mình là LUÔN LUÔN dùng option -t này nhé. Phần tên thì các bạn có thể để tuỳ ý. Ở đây mình lấy là learning-docker/node và gán cho nó tag là v1, ý chỉ đây là phiên bản số 1. Nếu ta không gán tag thì mặc định sẽ được để là latest nhé.
- Cuối cùng mình có 1 dấu "chấm" ý bảo là Docker hãy build Image với context (bối cảnh) ở folder hiện tại này cho tôi. Và Docker sẽ tìm ở folder hiện tại Dockerfile và build.
# 4. Chạy project từ Docker Image:
Để chạy project với docker-compose, ta tạo một file mới với tên là docker-compose.yml, vẫn ở folder "bai1-nodejs" nhé, với nội dung như sau:
```yml
version: "3.8"
services:
  app:
    image: learning-docker/node:v1
    ports:
      - "3000:3000"
    restart: unless-stopped
```
## 4.1. Giải thích:
- Đầu tiên ta định nghĩa version (phiên bản) của file cấu hình docker-compose, lời khuyên là luôn chọn phiên bản mới nhất. Các bạn có thể xem ở đây nhé
- Tiếp theo là ta có services, bên trong services ta sẽ định nghĩa tất cả các thành phần cần thiết cho project của bạn, ở đây ta chỉ có 1 service tên là app với image là tên image các ta vừa build
- Ở trong service app ta có trường restart ở đây mình để là unless-stopped, ý bảo là tự động chạy service này trong mọi trường hợp (như lúc khởi động lại máy chẳng hạn), nhưng nếu service này bị dừng bằng tay (dừng có chủ đích), hoặc đang chạy mà gặp lỗi bị dừng thì đừng restart nó. Vì khả năng cao khi ta tự dừng có chủ đích là ta muốn làm việc gì đó, hay khi gặp lỗi thì ta cũng muốn tìm hiểu lỗi là gì trước khi khởi động lại 😉
- Ở trên mình đã thêm vào 1 thuộc tính là ports để ta map các cổng (port) cần thiết. Ở đây ta chỉ map 1 cặp cổng 3000 (ở hệ môi trường gốc) vào cổng 3000 ở trong container. Chú ý bên trái là môi trường gốc (bên ngoài), bên phải là cổng của container. Ở đây cổng ở môi trường gốc ta có thể chọn tuỳ ý, nhưng cổng của container thì phải là 3000, project NodeJS của chúng ta chạy trong container ở cổng 3000. Do đó các bạn có thể thay đổi như: "3001:3000" hay "5000:3000", tuỳ ý nhé, nhưng thường mình sẽ để cổng giống nhau luôn.
- Cuối cùng là chạy project thôi nào. Các bạn dùng command sau:
**docker compose up -d**
- Muốn dừng các container đang chạy, vẫn ở trong thư mục "bai1-nodejs" các bạn dùng command sau:
**docker compose down**