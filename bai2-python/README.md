<h1>SERIES TỰ HỌC DOCKER - BÀI 1 - DOCKERIZE NODEJS</h1>

- [1. Mở đầu](#1-mở-đầu)
- [2. Cấu hình Dockerfile:](#2-cấu-hình-dockerfile)
  - [2.1. Giải thích file cấu hình bên trên:](#21-giải-thích-file-cấu-hình-bên-trên)
- [3. Build Docker Image:](#3-build-docker-image)
  - [3.1. Giải thích câu lệnh trên:](#31-giải-thích-câu-lệnh-trên)
- [4. Chạy project từ Docker Image:](#4-chạy-project-từ-docker-image)
  - [4.1. Giải thích:](#41-giải-thích)
- [5. Dọn dẹp tài nguyên:](#5-dọn-dẹp-tài-nguyên)
---
# 1. Mở đầu 
Trước khi bắt đầu **dockerize** ứng dụng các bạn nhớ kiểm tra cài đặt **docker** và **docker compose** chưa nhé. Nếu chưa cài các bạn có thể xem hướng dẫn tại đây:
[Install docker ](https://docs.docker.com/engine/install/ubuntu/)
[Install docker compose ](https://docs.docker.com/compose/install/linux/#install-using-the-repository)
Version OS, Docker, Docker compose hiện tại đang dùng:
> - Ubuntu 22.04.1 LTS<br>
> - Docker version 20.10.21, build baeda1f<br>
> - Docker Compose version v2.12.2

Để tạo ra Image chúng ta sẽ viết cấu hình cho Image ở trong 1 file gọi là Dockerfile, trong đó sẽ đầy đủ chi tiết những thứ như: môi trường gì (ubuntu, win, Centos, debian,...), php mấy, database,....

Bắt tay vào làm thôi nhé.
# 2. Cấu hình Dockerfile:
Ở thư mục docker-python các bạn tạo file tên là Dockerfile, bên trong có nội dung như sau:
```yml
FROM python:3.11-alpine
WORKDIR /app
COPY . /app
RUN pip install -r requirements.txt
CMD ["python","app.py"]
```
## 2.1. Giải thích file cấu hình bên trên:
- Dòng đầu tiên FROM: ta bắt đầu từ 1 Image có môi trường Alpine và đã cài sẵn Python phiên bản 3.11 . Xem danh sách Image Python ở đâu, các bạn check ở [link chính thức](https://hub.docker.com/_/python) này nhé.
- Lí do sao lại chọn Alpine mà không phải Ubuntu hay Debian, CentOS,.... Thì ở bài trước ở mục này mình đã phân tích rồi nhé. Đồng thời xuyên suốt series này mình sẽ luôn dùng môi trường hệ điều hành Alpine Linux nhé.
- Tiếp theo trong file Dockerfile ta có WORKDIR: ý là ta sẽ chuyển đến đường dẫn là /app bên trong Image, nếu đường dẫn này không tồn tại thì sẽ tự động được tạo luôn nhé.
- Tiếp theo ta COPY toàn bộ file từ folder ở môi trường gốc (bên ngoài - folder docker-python) và đưa vào trong đường dẫn /app bên trong Image.
- Tiếp tới là ta cài đặt dependencies, cần cài những thứ gì thì ta đề cập sẵn ở file requirements.txt rồi (câu lệnh này các bạn có thể xem nó xêm xêm như npm install trong NodeJS nhé).
- Cuối cùng là ta dùng CMD để chỉ command mặc định khi một container được khởi tạo từ Image: ở đây ta sẽ khởi động file app.py .
# 3. Build Docker Image:
Để build image trong Docker bằng Dockerfile ta chạy lệnh sau:
```docker build -t tuhoc-docker/python:v1 .```

## 3.1. Giải thích câu lệnh trên:
- Ở bài trước mình đã giải thích cho các bạn command trên làm gì rồi, các bạn có thể xem lại nhé.

- Giải thích nhanh: command trên sẽ build 1 image tên là tuhoc-docker/python với tag là v1, cả tên và tag ta đều có thể chọn tùy ý, nếu ta không để tag thì sẽ tự động được lấy là latest. Dấu "chấm" ở cuối ý bảo Docker là "tìm file Dockerfile ở thư mục hiện tại và build" nhé 😉

Sau khi quá trình build Image thành công, các bạn có thể kiểm tra bằng command:
``` docker images ```

Sẽ thấy hiển thị như sau nhé:
![docker-images](.\image\docker-images.png)
# 4. Chạy project từ Docker Image:
Để chạy project với docker-compose, ta tạo một file mới với tên là docker-compose.yml, vẫn ở folder "bai2-nodejs" nhé, với nội dung như sau:
```yml
version: '3.8'

services:
  app:
    image: tuhoc-docker/python:v1
    ports:
      - ${public_port}:${container_port}
    restart: unless-stopped
    environment:
      - container_port=${container_port}
```
## 4.1. Giải thích:
- Đầu tiên ta định nghĩa version (phiên bản) của file cấu hình docker-compose, lời khuyên là luôn chọn phiên bản mới nhất. Các bạn có thể xem ở đây nhé
- Tiếp theo là ta có services, bên trong services ta sẽ định nghĩa tất cả các thành phần cần thiết cho project của bạn, ở đây ta chỉ có 1 service tên là app với image là tên image ta vừa build ở **Dockerfile** phía trên nhé.
- Ở trong service app ta có trường restart ở đây mình để là unless-stopped, ý bảo là tự động chạy service này trong mọi trường hợp (như lúc khởi động lại máy chẳng hạn), nhưng nếu service này bị dừng bằng tay (dừng có chủ đích), hoặc đang chạy mà gặp lỗi bị dừng thì đừng restart nó. Vì khả năng cao khi ta tự dừng có chủ đích là ta muốn làm việc gì đó, hay khi gặp lỗi thì ta cũng muốn tìm hiểu lỗi là gì trước khi khởi động lại 😉
- Ở trên mình đã thêm vào 1 thuộc tính là port để ta map các cổng (port) cần thiết. Ở đây ta chỉ map 1 cặp cổng **public_port** ở hệ môi trường gốc vào cổng **contianer_port** ở trong container. Giá trị **public_port** và **container_port** sẽ được khai báo trong file **.env**
- Ở folder docker-python ta tạo file **.env** với nôi dung như sau:
  ```yml
  container_port=6666
  public_port=7777
  ```
- Giải thích:
  - Biến container_port: chỉ port của project chạy bên trong container.
  - Biến public_port: chỉ port mà "thế giới bên ngoài" dùng để truy cập vào project 😉 (ý là port ta gọi ở trình duyệt).
  - Giá trị container_port và public_port trong file **.evn** ta có thể đặt tuỳ ý.
- Ở file app.py chúng ta chúng ta cũng phải import thêm thư viện **os** và khai báo thêm **port** cho container, chi tiết toàn bộ code file app.py như sau:
  ```python
  from flask import Flask, render_template
  import os # thêm thư viện OS  

  app = Flask(__name__)

  @app.route("/")
  def hello():
      return render_template('index.html', title='Docker Python', name='James')

  if __name__ == "__main__":
      app.run(host="0.0.0.0", port=os.environ['container_port']) # khai báo port container từ biến môi trường trong file .env
  ```
- Cuối cùng là chạy project thôi nào. Các bạn dùng command sau:
  ```docker compose up -d``` 
  Chúng ta sẽ thấy docker compose tạo ra 1 network và 1 container như sau:  
  ![docker-compose-up](.\image\docker-compose-up.png)

- Muốn thay đổi port cho project, các bạn chỉ cần khai báo hai giá trị **container_port** và **public_port** trong file **.env** và chạy lệnh: 
  ```
  docker compose down
  docker compose up -d  
  ```
  ![docker compose down](.\image\docker-compose-down.png)
  ![docker compose up](image/docker-compose-up.png)
- Để kiểm tra các container đã chạy, chúng ta dùng lệnh sau: 
  ```docker compose ps -a```
  ![docker compose ps -a](.\image\docker-compose-ps.png)
  
- Muốn check logs docker compose các bạn gõ lệnh: 
  ```docker compose logs -f```
  ![docker compose logs -f](.\image\docker-compose-logs.png)
  Để ý logs sau khi chạy docker compose up, chúng ta thấy port của container đã được thay đổi thành port 6666 (mặc định python chạy port 5000), port public là 7777 đúng với các giá trị ta đã khải báo trong file **.env**
- Kết quả: 
  Chúng ta gõ địa chỉ server và port như hình cho kết quả như bên dưới.
![docker resuilt](.\image\docker-resuilt.png)
# 5. Dọn dẹp tài nguyên:
- Sau khi thành công Dockerize ứng dụng Python, chúng ta tiến hành dọn dẹp tài nguyên bằng các lệnh sau.
  ```
  docker compose down
  docker system prune -af
  ```
- Giải thích:
  - Lênh ```docker compose down``` Để dừng các container đang chạy và remove các card mạng lúc chạy docker compose tạo ra.
  - Lệnh ```docker system prune -af``` sẽ xoá các tài nguyên:  
    - tất cả các container đã dừng.
    - tất cả các mạng không được kế nối với container để sử dụng.
    - tất cả hình ảnh không có liên kết với container.
    - tất cả bộ đệm xây dựng.

    ![docker clean](.\image\docker-clean.png)