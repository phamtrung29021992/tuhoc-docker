from flask import Flask, render_template
import os # thêm thư viện OS  

app = Flask(__name__)

@app.route("/")
def hello():
    return render_template('index.html', title='Docker Python', name='James')

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=os.environ['container_port']) # khai báo port container từ biến môi trường trong file .env